# Development Task in OOP PHP 7
---
## Deploy Instructions
---

### Installation
1. Install docker and docker-compose:
    * Go [here](https://www.docker.com/get-started) and download it for your platform.
    * *(Only for Linux)* Also download docker-compose from [here](https://docs.docker.com/compose/install/#install-compose-on-linux-systems).
2. Clone this repository
3. Navigate to `docker` folder via command line and run:
`docker-compose up -d --build` to build a detached composition.
4. Install `composer` for the project
    * [Attach](#console) to the running container console and run `composer install` **or** run `docker exec -it task-oop-kiss composer install` , *where `task-oop-kiss` is the container's name.*
        * *(Only for Linux)* Reclaim your folder ownership on your host machine, because the newly created `vendor` folder belongs to `root`. Run ``sudo chown `whoami` -R task-oop-kiss`` *(If you are already inside the project's `docker` folder, replace `task-oop-kiss` with `../`)*.
    * *(Alternatively, but only for Linux)* Run `post-installation.sh` script inside the `docker` folder.

### Destruction
To destroy **all** your compositions **INCLUDING EXISTING**, run: `docker-compose down`

### Interaction
#### Console
This `docker-compose.json` file names the container of the app as `task-oop-kiss`.
In order to access it, simply run: `docker exec -it task-oop-kiss bash` to gain access to a bash console inside the container.
Then you can simply run `php app/index.php`.

### Interaction 
#### Browser
The apache server is set up on `localhost` at port `8000`. Go to `localhost:8000/app` or `127.0.0.1:8000/app` from your browser.
If this port is reserved, then edit the `Dockerfile` and use another port accordingly and start over.

### Useful
Set `"config": { "optimize-autoloader": true }` in production and `false` in development.

---

## Task
---
### Business requirements
1. Write a class with a method that takes 2 integers between 1 and 100.
2. Loop from the first integer to the second integer.
3. Write out each integer.
4. If the integer is divisible by 3 also print out “Foo”
5. If the integer is divisible by 5 also print out “Bar”

### Development requirements
1. Upload project in an online Git-based repository (Bitbucket)
2. PHP 7.* in Strict Mode
3. Composer package manager
4. PHPUnit testing framework
5. Before you write any other code write some unit tests

### Pay attention to
* Code clarity and ease of understanding
* Well commented code that gives clear and appropriate explanations
* Code that shows an understanding of Enterprise Software development requirements. i.e. we are just as interested in failure modes as in the happy path.

### Tutorial link
[Full PHP Tutorial for Beginners & Advanced](https://www.youtube.com/watch?v=sVbEyFZKgqk&list=PLr3d3QYzkw2xabQRUpcZ_IBk9W50M9pe-)