<?php

declare(strict_types=1);

namespace App;

use DomainException;

class Looper
{
    const EOL = PHP_EOL;
    const START_BEGIN = 1;
    const START_END = 100;
    const STOP_BEGIN = self::START_BEGIN;
    const STOP_END = self::START_END;

    public function loop(int $start, int $stop)
    {
        $this->validateInputRange($start, $stop);

        for ($i = $start; $i <= $stop; $i++) {
            echo $this->processText($i);
        }
    }

    /**
     * Verify that the input ranges given are nominal.
     * Warn or throw exception depending on case.
     * In our case the inclusive ranges are hardcoded as constants.
     */
    private function validateInputRange(int $start, int $stop): void
    {
        if (!$this->isInRange($start, self::START_BEGIN, self::START_END)) {
            throw new \DomainException(
                "Start (" . $start . ") is out of pre-defined domain ("
                    . self::START_BEGIN . "-" . self::START_END . ")." . self::EOL
            );
        }

        if (!$this->isInRange($stop, self::STOP_BEGIN, self::STOP_END)) {
            throw new \DomainException(
                "Stop (" . $stop . ") is out of pre-defined domain ("
                    . self::STOP_BEGIN . "-" . self::STOP_END . ")." . self::EOL
            );
        }

        $this->sanityCheck($start, $stop);
    }

    /**
     * Check if the a given value is inside given range.
     */
    private function isInRange(int $value, int $min, int $max): bool
    {
        return ($value >= $min and $value <= $max);
    }

    /**
     * Verify that the user is aware that the loop will not occur.
     */
    private function sanityCheck(int $start, int $stop): void
    {
        if ($start > $stop) {
            throw new \RangeException("Start (" . $start . ")" . " cannot be greater than stop (" . $stop . ")!");
        }
    }

    /**
     * Ask the looper if the item is divisible by
     * a certain number, then construct the text
     * accordingly.
     */
    private function processText(int $i): string
    {
        $text = $i;

        if ($i % 3 === 0) {
            $text .= " Foo";
        }

        if ($i % 5 === 0) {
            $text .= " Bar";
        }

        // Return the final text.
        return $text . self::EOL;
    }
}
