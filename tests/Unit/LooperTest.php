<?php

declare(strict_types=1);

use App\Looper;
use PHPUnit\Framework\TestCase;

class LooperTest extends TestCase
{
    public function edgeCases(): array
    {
        return [
            [0, 0],
            [0, 101],
            [-100, 200],
        ];
    }

    /**
     * @test 
     * @dataProvider edgeCases
     */
    public function valuesAreInPermittedRange(int $start, int $stop): void
    {
        /**
         * Given
         * we have a loop that iterates over a
         * single number.
         */
        $looper = new Looper();

        /**
         * When
         * we expect some out of range values
         * such as starting numbers less than 1
         * and stopping numbers greater than 100.
         */
        $this->expectException(OutOfRangeException::class);

        /**
         * Then
         * we request start the problematic loop.
         */
        $looper->loop($start, $stop);
    }

    public function printCases(): array
    {
        return [
            [
                1, 5,
                "1" . PHP_EOL .
                    "2" . PHP_EOL .
                    "3 Foo" . PHP_EOL .
                    "4" . PHP_EOL .
                    "5 Bar" . PHP_EOL
            ],
            [
                10, 15,
                "10 Bar" . PHP_EOL .
                    "11" . PHP_EOL .
                    "12 Foo" . PHP_EOL .
                    "13" . PHP_EOL .
                    "14" . PHP_EOL .
                    "15 Foo Bar" . PHP_EOL
            ]
        ];
    }

    /**
     * @test 
     * @dataProvider printCases
     */
    public function loopPrintsExpectedOutput(
        int $start,
        int $stop,
        string $expected
    ): void {
        /**
         * Given
         * we have a loop that iterates over a
         * single number.
         */
        $looper = new Looper();

        /**
         * When
         * we print the contents of the looper.
         */
        $looper->loop($start, $stop);

        /**
         * Then
         * we confirm that output is exactly as expected.
         */
        $this->expectOutputString($expected);
    }

    public function typeErrorCases(): array
    {
        return [
            [0, '0'],
            [false, true],
            ['-100', 'test'],
            [5, array('23' => 5)]
        ];
    }

    /**
     * @test 
     * @dataProvider typeErrorCases
     */
    public function valuesAreIntegers($start, $stop): void
    {
        /**
         * Given
         * we have a loop that iterates over a
         * single number.
         */
        $looper = new Looper();

        /**
         * When
         * we expect some bogus input values that
         * are not integers.
         */
        $this->expectError(TypeError::class);

        /**
         * Then
         * we request start the problematic loop.
         */
        $looper->loop($start, $stop);
    }
}
