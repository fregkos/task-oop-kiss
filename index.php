<?php

declare(strict_types=1);

use App\Looper;

/*
* We go the grand-parent folder first,
* vendor is on the same folder as src (aka html folder)
*/

require_once __DIR__ . '/vendor/autoload.php';

try {
    (new Looper())->loop(1, 100);
} catch (OutOfRangeException $oor) {
    echo $oor->getMessage();
} catch (Exception $e) {
    echo $e->getMessage();
} catch (Error $err) {
    echo $err->getMessage();
}
